using Unicode

function palindromo(string)
    c = string
    b = Unicode.normalize(lowercase(replace(string, r"[\-',!;?/|. ^]" => "")),stripmark = true)
    a = length(b)
    f = 0
    for i = 1:a
        if(b[i] != b[a-i+1])
            #println(b[i]," é diferente de ", b[a-i+1])
            f = + 1 
        end
    end
    f > 0 ? false : true
end

using Test

function test()
    @test palindromo("")
    @test palindromo("ovo")
    @test palindromo("Socorram-me, subi no ^onibus em Marrocos!")
    @test palindromo("Some men interpret nine memos")
    @test palindromo("Gateman sees name, garageman sees name tag")
    @test palindromo("A mala nada na lama.")
    @test palindromo("A grama é amarga.")
    @test palindromo("I did, did I?")
    @test palindromo("Red rum, sir, is murder")
    @test palindromo("Eva, can I see bees in a cave?")
    @test palindromo("Go Hang a Salami! I'm a Lasagna Hog")
    @test palindromo("Olé! Maracujá, caju, caramelo!")
    @test palindromo("Rir, o breve verbo rir.")
    @test palindromo("Anotaram a data da maratona.")
    @test palindromo("Go deliver a dare, vile dog")
    @test !palindromo("Passei em MAC0110!")
    println("Fim dos testes!")
end


test()